from Classes.Card import Card

class NormalMonster(Card):
  def __init__(self, name, description, stars, type, attackPoints, defensePoints, hasEffect, nature):
    self.name = name
    self.description = description
    self.stars = stars
    self.type = type
    self.attackPoints = attackPoints
    self.defensePoints = defensePoints
    self.hasEffect = hasEffect
    self.nature = nature

  def attack(self):
    print ("attack")

"https://db.ygoprodeck.com/api/v7/cardinfo.php?name=Tornado%20Dragon"

monster = NormalMonster(
  "Blue Eyes White Dragon", 
  "lorem", 8, 
  "dragon", 
  3000, 
  2500, 
  False, 
  "Light"
)

monster.attack()